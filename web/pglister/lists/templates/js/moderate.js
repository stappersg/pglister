/*
 * We need a serviceworker to be homescreenable,
 * even if we don't (yet) support anything offline anyway.
 */

if ('serviceWorker' in navigator) {
  navigator.serviceWorker
           .register('./moderate-worker.js', {
	       'scope': '/moderate/',
	   })
           .then(function() {
	       console.log('Service Worker Registered');
	   });
}


/*
 * Set up popovers
 */
$(function () {
  $('[data-toggle="popover"]').popover({container: 'body'});
});

/* Show/hide reject reason box */
$('select[name^=act_]').change(function(){
    var name = $(this)[0].name
    var id = name.substr(4);
    if ($(this).val() == '4') {
      $('input[name=r_' + id + ']').show();
    } else {
      $('input[name=r_' + id + ']').hide();
    }
});
