Double-click on an email from the mailing list and open it in a new window.
Go to 'File' then 'Properties' and that will open up a new window.  Towards
the bottom of that window should be a field called "Internet headers" which
should include the 'List-Unsubscribe' header.  Copy and paste the URL shown
into your browser and follow the instructions provided.
