# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import pglister.lists.models


class Migration(migrations.Migration):

    dependencies = [
        ('lists', '0002_unique_subscriptions'),
    ]

    operations = [
        migrations.AddField(
            model_name='list',
            name='moderation_regex',
            field=models.TextField(blank=True, validators=[pglister.lists.models.ValidateRegexpList]),
        ),
    ]
