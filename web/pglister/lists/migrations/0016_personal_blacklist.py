# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('lists', '0015_global_blacklist'),
    ]

    operations = [
        migrations.AddField(
            model_name='subscriberaddress',
            name='blacklisted',
            field=models.BooleanField(default=False),
        ),
    ]
