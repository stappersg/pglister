from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.db import connection
from django.template.loader import get_template
from django.conf import settings

import os

from lib.mailutil.simple import make_simple_mail
from pglister.listlog.util import log, Level


class LoginRequired(object):
    @classmethod
    def as_view(cls):
        return login_required(super(LoginRequired, cls).as_view())


def SimpleWebResponse(request, title, txt):
    return render(request, 'simple.html', {
        'title': title,
        'txt': txt,
    })


def send_template_mail(templatename, senderaddr, sendername, recipientaddr, recipientname, subject, contextdict={}, headers={}, autosubmitted='auto-generated'):
    template = get_template(templatename)
    msg = make_simple_mail(senderaddr,
                           sendername,
                           recipientaddr,
                           recipientname,
                           subject,
                           template.render(contextdict),
                           headers=headers,
                           autosubmitted=autosubmitted,
                           )
    curs = connection.cursor()
    curs.execute("INSERT INTO admin_out(sender, recipient, contents) values (%(sender)s, %(recipient)s, %(contents)s)", {
        'sender': settings.CONFIRM_SENDER_ADDRESS,
        'recipient': recipientaddr,
        'contents': msg.as_string(),
    })
    curs.execute("NOTIFY admin_out")


def send_template_mail_to_moderators(l, templatename, subject, contextdict):
    contextdict.update({
        'list': l,
    })
    for m in l.moderators.all():
        send_template_mail(
            templatename,
            l.moderator_notice_address(),
            l.moderator_notice_name(),
            m.user.email,
            m.fullname,
            subject,
            contextdict,
        )


def get_unsubscribe_instructions():
    root = os.path.join(os.path.dirname(__file__), 'templates/unsubscribe_instructions/')
    for fn in sorted(os.listdir(root)):
        if not fn.endswith('.txt'):
            continue
        with open(os.path.join(root, fn)) as f:
            yield (fn[:-4], f.read().replace("\n", ' '))


def complete_subscription(user, list, addr):
    # Remove from whitelist, if was previously on whitelist
    curs = connection.cursor()
    curs.execute("DELETE FROM lists_listwhitelist WHERE list_id=%(listid)s AND address=%(addr)s RETURNING id", {
        'listid': list.id,
        'addr': addr,
    })
    if curs.rowcount:
        log(Level.INFO, user, "Removed {0} from whitelist on {1}, address is now subscribed".format(addr, list))

    # If there are any emails in the queue for this user, that are moderated because the user was not
    # a subscriber (of this or any other list), approve them.
    curs.execute(
        """UPDATE moderation SET approved='t' WHERE approved='f'
AND (reason=2 OR (reason=1 AND listid=%(listid)s))
AND fromaddr=%(fromaddr)s RETURNING messageid""",
        {
            'listid': list.id,
            'fromaddr': addr,
        })

    if curs.rowcount:
        for m, in curs.fetchall():
            log(Level.INFO, user, 'Approved post from {0} to {1} due to subscription to the list.'.format(addr, list), m)
        curs.execute("NOTIFY moderation")

    # Send notificatios if enabled
    if list.notify_subscriptions:
        send_template_mail_to_moderators(
            list,
            'mail/subscription_notify.txt',
            'Subscription to {0}'.format(list),
            {
                'email': addr,
            },
        )
