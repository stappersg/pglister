from django.db.models import Q
from django.db import transaction
from django.core.exceptions import PermissionDenied
from django.contrib.auth import logout

import re

from lib.baselib.misc import generate_random_token

from .models import Subscriber, SubscriberAddress


def handle_login(sender, user, request, **kwargs):
    """
    Gets called whenever a user logged in.

    We use this to migrate information from the old systems. This is
    done by simply matching up any existing subscriptions with the
    exact same email address, so they get added automatically.
    It also matches addresses of the format foo+<x>@barcom to foo@bar.com.

    We can safely do this, because "upstream" community authentication
    has validated the ownership of the email address already.
    """

    (name, domain) = request.user.email.split('@')

    subaddrs = list(SubscriberAddress.objects.filter(
        Q(email=request.user.email.lower()) |
        Q(email__regex=r'^{0}\+[^@]+@{1}$'.format(re.escape(name), re.escape(domain))),
    ))

    if subaddrs:
        # One or more addresses exist matching the current user logging in.
        #
        # If any of these addresses are attached to a *different* account
        #   already, we cannot complete the migration.
        # If they are attached to the same account, everything is fine.
        # If they are attached to no account, we need to attach them.

        with transaction.atomic():
            if not hasattr(request.user, 'subscriber'):
                # Current user does not have a subscriber entry, which means that if there
                # is any subscriber on any of the addresses, it is somebody else.
                for sa in subaddrs:
                    if sa.subscriber is not None:
                        # Force django logout, so we end up re-running this migration
                        # on retries
                        e = request.user.email.lower()
                        logout(request)
                        raise PermissionDenied("Your primary email address ({}) is already attached to a different account on this server.".format(e))
                s = Subscriber(user=request.user)
                s.save()
            else:
                # Current user *does* have a subscriber entry. Make sure that all addresses
                # are the same or none, and don't belong to anybody else.
                s = request.user.subscriber
                for sa in subaddrs:
                    if sa.subscriber != s and sa.subscriber is not None:
                        # Force django logout, so we end up re-running this migration
                        # on retries
                        e = request.user.email.lower()
                        logout(request)
                        raise PermissionDenied("Your primary email address ({}) is already attached to a different account on this server.".format(e))

            # Attach the current subscriber to any unattached ones
            for sa in subaddrs:
                if sa.subscriber is None:
                    sa.subscriber = s
                    sa.save()
    else:
        # No existing subscriber addresses. However, we still need to
        # create a subscriber record for this user so the site works.
        # We also create a subscriber address for whatever email is on
        # the community account
        if not hasattr(request.user, 'subscriber'):
            with transaction.atomic():
                s = Subscriber(user=request.user)
                s.save()
                SubscriberAddress(subscriber=s, email=request.user.email, confirmed=True, token=generate_random_token()).save()
