from django.db import connection


def exec_to_list(query, params=None):
    curs = connection.cursor()
    curs.execute(query, params)
    return curs.fetchall()


def exec_to_row(query, params=None):
    curs = connection.cursor()
    curs.execute(query, params)
    return curs.fetchone()


def exec_to_dict(query, params=None):
    curs = connection.cursor()
    curs.execute(query, params)
    columns = [col[0] for col in curs.description]
    return [dict(zip(columns, row))for row in curs.fetchall()]


def exec_to_scalar(query, params=None):
    curs = connection.cursor()
    curs.execute(query, params)
    r = curs.fetchone()
    if r:
        return r[0]
    # If the query returns no rows at all, then just return None
    return None


def exec_to_scalar_dict(query, params=None):
    return exec_to_dict(query, params)[0]


def exec_to_hier(query, params=None):
    # XXX: make configurable to do this at more than one level?
    # XXX: assumes that query comes sorted by first columns, and that
    #      the first column is unique.
    curs = connection.cursor()
    curs.execute(query, params)
    columns = [col[0] for col in curs.description[1:]]
    data = {}
    for row in curs:
        data[row[0]] = dict(zip(columns, row[1:]))
    return data
